# Class

> Đáp binary vào IDA coi. Nhìn qua tôi đoán là khai thác trên heap

## Hàm create

> Tôi cũng luôn đọc hàm create đầu tiên bởi vì nó cho tôi biết cấu trúc *strunk heap* được tổi chức ntn?

![create](img/create.PNG)

* Line 13 `v4 = malloc(8u)` gọi strunk_0 
    - v4[0] chứa địa chỉ của struct_1
    - v4[1] chứa định danh `v4[1] = i + 1`

* Line 23 `v1 = malloc(128u)` gọi struct_1
    - 120 byte đầu tiên sẽ chứa tên.( Max = 120)
    - 120 byte sau sẽ chứa độ dài của tên.

-> Tôi tạm xây dựng một struct như sau:
```
struct person
{
    int *name, // lưu địa chỉ malloc(120)
    int id
}
```

## Hàm Edit
> Tiếp theo tôi cũng sẽ luôn đọc hàm edit coi xem cách edit có điểm nào không đúng và có thể khai thác. Nếu không có tỗi sẽ chuyển sang đọc hàm delete(thường chứa khá nhiều). Nhưng thật may mắn tôi không cẩn phải đọc hàm delete vì sau các bạn biết rồi đấy.

![edit](img/Edit.PNG)

1. Đầu tiên nó đọc index.
2. Lấy độ dài của biến name cũ `v4 = *(_DWORD *)(v3 + 120);`
3. Tiếp đến là đọc name bằng đoạn code sau:
```
for ( i = 0; ; ++i )
      {
        result = i;
        if ( i > v4 ) // Haha i=121 < v4 bới break -> vậy tôi có thể chền 1byte nhưng
          break;
        result = getchar();
        if ( (_BYTE)result == '\n' )
          break;
        *(_BYTE *)(v3 + i) = result;
      }
```
* Haha i=121 < v4 bới break -> vậy tôi có thể chèn 1byte nhưng thật là may mắn tôi 1byte đó lại nằm trong vùng nhớ chứa độ dài của name.

--> Vậy tôi có thể ghi đè stunk_2->name;

## Ý tưởng

> Từ những dữ kiện đề bài cho ta có:

* Ghi đè strunk_2->name = table_got
* Dựa dẫm hàm *display* để leak libc addresss -> system_address or one_gadget_address
* Ghi đè bảng got bằng cách gọi hàm *edit* ghi lên strunk_3


## Mã Khai Thắc
[exploit](class.py)
