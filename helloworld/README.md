# PWN HelloWorld

> Đầu tiên và bao giờ cũng thế tôi sẽ load file binary vào IDA và đọc hiểu chương trình. Xem qua thì thấy chương trình bị lỗi *buffer overflow* trên stack và leak địa chỉ bất kì trên stack. Để hiểu rõ ntn mình sẽ trình bày phía dưới đây.

## Hàm mygame

![mygame](img/mygame.PNG)

* Tại line 35 `v2 = read(0, s1, 128u);` hàm read đọc quá độ dài cho phép của biến s1. (Tôi có thể thực hiện ghi đè *$EIP* nhảy đến một nơi bất kì). 
* Nhưng chương trình lại được bảo vệ bởi *stack cookie* `v1 = *MK_FP(__GS__, 20) ^ v5;`
--> Do đó để có thể ret đến một nơi tùy ý thì tôi phải leak dk stack cookie và libc address.

> Tiếp tục coi chương trình xem có đoạn nào có thể leak đươc stack cookie.

## Hàm helloworld

> Thật may mắn tại đây tôi có thể leak được stack cookie

![helloword](img/helloworl.PNG)

* `v2 = read(0, buf, 97u);` Đoạn code read thừa một byte, vừa hay tôi có thể tràn tới 1byte stack cookie và để lấp byte \x00
* ` printf("Hello %s, welcome to my home.\n", buf);` Hàm printf sẽ in chuỗi buf cho tớ khi gặp byte \x00 và in ra stack cookie.
* Vậy là tôi đã leak được stack cookie.

### NOTE: 

* Khi truyền tham số tôi cần chú ý đẩy đủ 97 byte. Nếu gặp '\n' nó sẽ set end_buf=\x00 và tôi sẽ không thể leak stack cookie
```
if ( buf[v2 - 1] == 10 )
    buf[v2 - 1] = 0;
``` 

* Tại dòng 22: Tôi cần phải return 1; Để hàm main có thể tiếp tục gọi hàm mygame.
```
  read(0, buf, 97u);
  v3 = atoi(buf);
  if ( v3 == 20 )
  {
    printf("Welcome to my home in %d.\n", 20);
    fflush(_bss_start);
    result = 1;
  }
```
Khi truyền tham sô tôi sẽ đẩy một lần để tránh byte "\x10" và tại read_2 tôi phải truyền  `"20" + \x00` để v3 == 20


--> Nhưng để lên được /bin/sh. Tôi lại không có địa chỉ hàm system or one+_gadget Tôi nảy ra một ý tưởng leak address libc:

## Ý tưởng leak libc adddress

* puts(bẳng_got)  (leak libc)
* ret address mygame (overflow thêm một lần nữa)

## Mã khai thác
[exploit](helloworld.py)
